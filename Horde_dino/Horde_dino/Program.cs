﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Horde
{
    class Program
    {
        static string getName(Horde_dino.Dinosaur dinosaur)
        {
            return dinosaur.name;
        }

        static string getSpecie(Horde_dino.Dinosaur dinosaur)
        {
            return dinosaur.specie;
        }

        static int getAge(Horde_dino.Dinosaur dinosaur)
        {
            return dinosaur.age;
        }

        static string setName()
        {
            Console.Write("Donner son nom : ");
            string nom = Console.ReadLine();
            return nom;
        }

        static string setSpecie()
        {
            Console.Write("Donner son espece : ");
            string specie = Console.ReadLine();
            return specie;
        }

        static int setAge()
        {
            int age;
            string age_string;
            do
            {
                Console.Write("Donner son age : ");
                age_string = Console.ReadLine();
            } while (!int.TryParse(age_string, out age));
            return age;
        }

        static void Main(string[] args)
        {
            Horde_dino.Dinosaur louis = new Horde_dino.Dinosaur("Louis", "Stegausaurus", 12);
            Horde_dino.Dinosaur nessie = new Horde_dino.Dinosaur("Nessie", "Diplodocus", 11);
            Horde_dino.Dinosaur jean_luc = new Horde_dino.Dinosaur("Jean_luc", "Pterodactile", 7);
            Horde_dino.Dinosaur mathilde = new Horde_dino.Dinosaur("Mathilde", "T-rex", 19);

            List<Horde_dino.Dinosaur> dinosaurs = new List<Horde_dino.Dinosaur>();

            dinosaurs.Add(louis); //Append dinosaur reference to end of list
            dinosaurs.Add(nessie);

            Console.WriteLine(dinosaurs.Count);
            //Iterate over our list
            foreach (Horde_dino.Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(dino.sayHello());
            }

            dinosaurs.RemoveAt(1); //Remove dinosaur at index 1

            Console.WriteLine(dinosaurs.Count);
            //Iterate over our list
            foreach (Horde_dino.Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(dino.sayHello());
            }

            dinosaurs.Remove(louis);

            Console.WriteLine(dinosaurs.Count);
            //Iterate over our list
            foreach (Horde_dino.Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(dino.sayHello());
            }
            dinosaurs.Add(mathilde);
            dinosaurs.Add(jean_luc);
            Console.ReadKey();
        }
    }
}
